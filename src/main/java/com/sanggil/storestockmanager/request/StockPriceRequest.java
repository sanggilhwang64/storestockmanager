package com.sanggil.storestockmanager.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StockPriceRequest {

    private Integer productPrice;

    @NotNull
    @Min(value = 1)
    @Max(value = 50)
    private Integer quantity;
}
