package com.sanggil.storestockmanager.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StoreStockItem {
    private Long id;

    private String barCode;

    private String productName;

    private Integer productPrice;

    private Integer quantity;

    private LocalDate expirationDate;

    private String StockStatus;




}
