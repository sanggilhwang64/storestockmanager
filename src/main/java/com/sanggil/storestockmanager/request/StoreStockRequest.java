package com.sanggil.storestockmanager.request;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class StoreStockRequest {
    @NotNull
    private String barCode;

    @NotNull
    @Length(min = 1, max = 20)
    private String productName;

    @NotNull
    private Integer productPrice;

    @NotNull
    @Min(value = 0)
    @Max(value = 50)
    private Integer quantity;

    @NotNull
    private String manufactureDate;

    @NotNull
    private LocalDate expirationDate;
}
