package com.sanggil.storestockmanager.repository;

import com.sanggil.storestockmanager.entity.StoreStock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreStockRepository extends JpaRepository<StoreStock, Long> {
}
