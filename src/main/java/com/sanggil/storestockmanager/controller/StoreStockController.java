package com.sanggil.storestockmanager.controller;

import com.sanggil.storestockmanager.request.StockPriceRequest;
import com.sanggil.storestockmanager.request.StoreStockItem;
import com.sanggil.storestockmanager.request.StoreStockRequest;
import com.sanggil.storestockmanager.service.StoreStockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/storestock")
public class StoreStockController {
    private final StoreStockService storeStockService;

    @PostMapping("/data")
    public String setStoreStock(@RequestBody @Valid StoreStockRequest request) {
        storeStockService.setStoreStore(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<StoreStockItem> getStore() {
        List<StoreStockItem> result = storeStockService.getStock();

        return result;
    }

    @PutMapping("/info/id/{id}")
    public String putStockInfo(@PathVariable long id, @RequestBody @Valid StockPriceRequest request) {
        storeStockService.putStockInfo(id, request);

        return "OK";
    }

    @DeleteMapping("/stock-out/id/{id}")
    public String delStock(@PathVariable long id) {
        storeStockService.delStock(id);

        return "OK";
    }
}
