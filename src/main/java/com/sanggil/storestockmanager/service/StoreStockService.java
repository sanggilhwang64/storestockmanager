package com.sanggil.storestockmanager.service;

import com.sanggil.storestockmanager.entity.StoreStock;
import com.sanggil.storestockmanager.repository.StoreStockRepository;
import com.sanggil.storestockmanager.request.StockPriceRequest;
import com.sanggil.storestockmanager.request.StoreStockItem;
import com.sanggil.storestockmanager.request.StoreStockRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreStockService {
    private final StoreStockRepository storeStockRepository;

    public void setStoreStore(StoreStockRequest request) {
        StoreStock addData = new StoreStock();
        addData.setBarCode(request.getBarCode());
        addData.setProductName(request.getProductName());
        addData.setProductPrice(request.getProductPrice());
        addData.setQuantity(request.getQuantity());
        addData.setManufactureDate(request.getManufactureDate());
        addData.setExpirationDate(LocalDate.now());

        storeStockRepository.save(addData);
    }
    public List<StoreStockItem> getStock() {
        List<StoreStock> originList = storeStockRepository.findAll();

        List<StoreStockItem> result = new LinkedList<>();

        for (StoreStock item : originList) {
            StoreStockItem addItem = new StoreStockItem();
            addItem.setId(item.getId());
            addItem.setBarCode(item.getBarCode());
            addItem.setProductName(item.getProductName());
            addItem.setProductPrice(item.getProductPrice());
            addItem.setQuantity(item.getQuantity());
            addItem.setExpirationDate(item.getExpirationDate());

            String statusName = "정상";
            if (item.getExpirationDate().isBefore(LocalDate.now())) {
                statusName = "폐기";
            } else if (item.getExpirationDate().minusDays(1).equals(LocalDate.now())) {
                statusName = "임박";
            }

            addItem.setStockStatus(statusName);

            result.add(addItem);
        }

        return result;
    }

    public void putStockInfo(long id, StockPriceRequest request) {
        StoreStock originData = storeStockRepository.findById(id).orElseThrow();
        originData.setProductPrice(request.getProductPrice());
        originData.setQuantity(request.getQuantity());

        storeStockRepository.save(originData);
    }

    public void delStock(long id) {
        storeStockRepository.deleteById(id);
    }
}
