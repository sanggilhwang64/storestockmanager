package com.sanggil.storestockmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreStockManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreStockManagerApplication.class, args);
    }

}
